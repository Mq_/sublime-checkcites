#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import hashlib
import os
import sys
from subprocess import Popen, PIPE
import re


def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("root", help="The root file of the tex document.")
    parser.add_argument("-a", "--accept", action="store_true",
                        help="Accept unused references in bibliography files and do not exit with an error status.")
    parser.add_argument("-d", "--directory", default=None, help="The directory to look for bibliography files.")
    parser.add_argument("-e", "--extension", default="bib", help="The file extension of the bibliography files.")
    parser.add_argument("-v", "--verbose", action="store_true", help="Be more verbose.")

    args = parser.parse_args()

    # forbid ".." in directory path
    if args.directory is not None and ".." in args.directory:
        print("Directory must not contain \"..\"")
        sys.exit(1)

    # Path to root document
    root = "{}/{}".format(os.getcwd(), args.root)

    if args.verbose:
        print("Root: {}".format(root))

    # hash of root document path. Used by sublime as cache folder name.
    root_hash = hashlib.md5(root.encode("utf-8")).hexdigest()

    if args.verbose:
        print("Root Hash: {}".format(root_hash))

    # path to cache folder
    cache = "{}/.config/sublime-text-3/Cache/LaTeXTools/{}".format(os.path.expanduser("~"), root_hash)

    if not os.path.exists(cache):
        cache = "{}/.cache/sublime-text/Cache/LaTeXTools/{}".format(os.path.expanduser("~"), root_hash)

    if not os.path.exists(cache):
        print("Can not locate cache.")
        sys.exit(1)

    if args.verbose:
        print("Cache: {}".format(cache))

    if not os.path.isdir(cache):
        print("{} does not exist.".format(cache))
        sys.exit(1)

    if args.directory is None:
        # bibfiles are located in same folder as root document
        # symlink every file ending with file extension
        for file in os.listdir():
            if file.endswith(args.extension):
                src = "{}/{}".format(os.getcwd(), file)
                dst = "{}/{}".format(cache, file)
                if os.path.exists(dst):
                    os.remove(dst)
                os.symlink(src, dst)
                if args.verbose:
                    print("Create symlink: {} -> {}".format(src, dst))
    else:
        # bibfiles are located in a subfolder
        # symlink whole folder
        src = os.path.abspath(args.directory)
        dst = os.path.abspath("{}/{}".format(cache, args.directory))
        if os.path.exists(dst):
            os.remove(dst)
        os.symlink(src, dst)
        if args.verbose:
            print("Create symlink: {} -> {}".format(src, dst))

    # go to cache directory and execute checkcites script
    if args.root[-4:] == ".tex":
        auxfile = "{}.aux".format(args.root[:-4])
    else:
        auxfile = "{}.aux".format(args.root)
    if args.verbose:
        print("Auxfile: {}".format(auxfile))
    p = Popen(["checkcites", auxfile], stdout=PIPE, cwd=cache)
    output = p.stdout.read().decode("utf-8")
    print(output)

    unused = int(re.search(r"Unused references in your .*: ([0-9]+)", output).group(1))
    undefined = int(re.search(r"Undefined references in your .*: ([0-9]+)", output).group(1))

    if args.verbose:
        print(f"{unused=} {undefined=}")

    if not args.accept and unused > 0 or undefined > 0:
        sys.exit(1)


if __name__ == '__main__':
    main()
