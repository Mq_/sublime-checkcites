# Sublime Checkcites

Helper script to integrate [Checkcites] into [Sublime Text] with [LaTeXTools].
Symlinks the required bibliography files to the cache directory used by [LaTeXTools] and executes [Checkcites].


[Sublime Text]: https://www.sublimetext.com/
[Checkcites]: https://github.com/cereda/checkcites
[LaTeXTools]: https://github.com/SublimeText/LaTeXTools
