# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- Add switch to accept unused references and use exit code `0`.

### Fixed

- Fix cache path lookup.
- Fix output parsing of checkcites.

## [1.0.0] - 2020-06-01

### Added

- Symlink bibliography files or folder containing bibligraphies to cache directory used by LaTeXTools.
- Execute `checkcites` in cache directory.
- Check `checkcites` output for unused and undefined references and return exit code `1` in case.

[Unreleased]: https://gitlab.com/Mq_/sublime-checkcites/compare/v1.0.0...master
[1.0.0]: https://gitlab.com/Mq_/sublime-checkcites/tags/v1.0.0
